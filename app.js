const expressh= require("express");
const bodyParser = require('body-parser');
const app=expressh();

var port= process.env.PORT || 3000;
app.set('view engine','pug'); //FOR USING PUG FILE
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let sortfunc = function(arrays) {
    console.log("here in my function");
    var frequency = {};
   var sortAble = [];
   var newArr = [];

   arrays.forEach(function(value) { 
       if ( value in frequency )
           frequency[value] = frequency[value] + 1;
       else
           frequency[value] = 1;
   });
   

   for(var key in frequency){
       sortAble.push([key, frequency[key]])
   }

   sortAble.sort(function(a, b){
       return b[1] - a[1]
   })

   
   sortAble.forEach(function(obj){
       for(var i=0; i < obj[1]; i++){
           newArr.push(obj[0]);
       }
   })


   var uniquearr = [];
       for (var i=0;i<newArr.length;i++){
         if ( uniquearr.indexOf(newArr[i]) == -1){
           uniquearr.push(newArr[i]);
         }
     }


   var counts = {};
     for (var i = 0; i < newArr.length; i++) {
         var num = newArr[i];
         counts[num] = counts[num] ? counts[num] + 1 : 1;
     }   


   var mainoutput="";
   for (var i=0;i<uniquearr.length;i++){
       mainoutput=mainoutput + uniquearr[i] + " is " + counts[uniquearr[i]] + " times " + "<br>";
   }
   return mainoutput;
 }

app.get('/sort',(req,res)=>{
    res.render('sort');
});
app.post('/sort',(req,res)=>{
  
   
   var ans=req.body.array;
   var arrays = JSON.parse(ans);
    //console.log(typeof(arrays));
   let mainoutput = sortfunc(arrays);

     /*Sending via Object-- START
     var result={};
     uniquearr.forEach((key,i)=> result[key] = counts[uniquearr[i]]);
     res.send(result);
     Sending via Object-- END*/

    res.render('output', { hello : mainoutput } )

});
app.get('/sort/:tagId',(req,res)=>{
    

    var ans=req.params.tagId;
    var arrays = JSON.parse(ans);
    let mainoutput = sortfunc(arrays);
    res.render('output', { hello : mainoutput } );
});



//ERROR HANDLING STARTS
app.use((req,res,next)=>{
    const err=new Error('Not Found');
    next(err);
});

app.use((err,req,res,next)=>{
    res.render('error');
});
//ERROR HANDLING ENDS

app.listen(port);